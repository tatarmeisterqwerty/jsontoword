﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonToWord
{
    partial class Program
    {
        static Justification GetJustification(string value)
        {
            var justification = new Justification();
            switch (value)
            {
                case "left":
                    {
                        justification.Val = JustificationValues.Left;
                    }break;
                case "right":
                    {
                        justification.Val = JustificationValues.Right;
                    }
                    break;
                case "center":
                    {
                        justification.Val = JustificationValues.Center;
                    }
                    break;
            }
            return justification;
        }
    }
}
