﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonToWord
{
    partial class Program
    {
        static ParagraphProperties GetParagraphStyle(JToken jToken)
        {
            var parPr = new ParagraphProperties();
            var jTokDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jToken.ToString());

            foreach (var item in jTokDict)
            {     
                switch(item.Key)
                {
                    case "textAlign":
                        {
                            parPr.Append(GetJustification(item.Value));
                        }
                        break;
                }
                
            } 
            return parPr;
        }
        static Paragraph GetParagraph(JToken content, JToken attrs)
        {
            var paragraph = new Paragraph();          
            var run = new Run();
            switch (content["type"].ToString())
            {
                case "hardBreak":
                    {
                        run.Append(new Text(""));
                        paragraph.Append(GetParagraphStyle(attrs));
                    }
                    break;
                case "text":
                    {
                        run.Append(new Text(content["text"].ToString()));
                        paragraph.Append(GetParagraphStyle(attrs));
                    }
                    break;

            }
            paragraph.Append(run);
            return paragraph;
        }
    }
}
