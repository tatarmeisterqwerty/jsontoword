﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonToWord
{
    partial class Program
    {
        static TableCellProperties GetTableCellStyle(JToken attrs)
        {
            var tableCellProp =  new TableCellProperties(
                        new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "20000" });

            var attrsDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(attrs.ToString());

            foreach (var attr in attrsDict)
            {
                switch(attr.Key)
                {
                    case "background":
                        {

                        }break;
                    case "colspan":
                        {

                        }break;
                    case "rowspan":
                        {

                        }break;
                    case "colwidth":
                        {

                        }break;
                }
            }
            return tableCellProp;
        }
        static Table GetTable(JToken content)
        {
            var table = new Table();

            var borderValue = BorderValues.Single;
            TableProperties tblProp = new TableProperties(
            new TableBorders(
                new TopBorder()
                {
                    Val =
                    new EnumValue<BorderValues>(borderValue),
                    Size = 5
                },
                new BottomBorder()
                {
                    Val =
                    new EnumValue<BorderValues>(borderValue),
                    Size = 5
                },
                new LeftBorder()
                {
                    Val =
                    new EnumValue<BorderValues>(borderValue),
                    Size = 5
                },
                new RightBorder()
                {
                    Val =
                    new EnumValue<BorderValues>(borderValue),
                    Size = 5
                },
                new InsideHorizontalBorder()
                {
                    Val =
                    new EnumValue<BorderValues>(borderValue),
                    Size = 5
                },
                new InsideVerticalBorder()
                {
                    Val =
                    new EnumValue<BorderValues>(borderValue),
                    Size = 5
                }
            )
            );
            table.AppendChild<TableProperties>(tblProp);

            foreach (var _item in content)
            {
                var item = _item["content"];
                var tableRow = new TableRow();
                foreach (var jCell in item)
                {
                    var tableCell = new TableCell();
                    tableCell.AppendChild(GetTableCellStyle(jCell["attrs"]));                    
                    foreach (var par in jCell["content"])
                    {
                        var str = par["content"]?.ToString();
                        if (!String.IsNullOrEmpty(str))
                            foreach (var para in par["content"])
                                tableCell.AppendChild(GetParagraph(para, par["attrs"]));
                        else tableCell.Append(new Paragraph(new Run(new Text(""))));    
                    }
                    tableRow.AppendChild(tableCell);
                }
                table.AppendChild(tableRow);
            }                       
            return table;
        }
    }
}
