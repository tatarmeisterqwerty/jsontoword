﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonToWord
{
    partial class Program
    {
        static void Main()
        {
            var filePath = @"C:\Users\max0l\OneDrive\Рабочий стол\JsonToWord\JsonToWord";
            var file = File.ReadAllText(filePath + @"\file.json");
            var jArray = JsonConvert.DeserializeObject<JArray>(file);
            
            // Create a document by supplying the filepath. 
            using (WordprocessingDocument wordDocument =
                WordprocessingDocument.Create(filePath + @"\doc.docx", WordprocessingDocumentType.Document))
            {
                // Add a main document part. 
                MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();
                
                // Create the document structure and add some text.
                mainPart.Document = new Document();
                Body body = mainPart.Document.AppendChild(new Body());


                foreach (var jObj in jArray)
                {
                    switch (jObj["type"].ToString())
                    {
                        case "paragraph":
                            {
                                foreach (var content in jObj["content"])
                                {
                                    body.AppendChild(GetParagraph(content, jObj["attrs"]));
                                }
                            }
                            break;
                        case "table":
                            {
                                body.AppendChild(GetTable(jObj["content"]));                                
                            }
                            break;
                    }
                }


                //
                

                //// Create a row.
                //TableRow tr = new TableRow();

                //// Create a cell.
                //TableCell tc1 = new TableCell();

                // Specify the width property of the table cell.
                //tc1.Append(new TableCellProperties(
                //    new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));

                // Specify the table cell content.
                //tc1.Append(new Paragraph(new Run(new Text("some text"))));

                //// Append the table cell to the table row.
                //tr.Append(tc1);

                //// Create a second table cell by copying the OuterXml value of the first table cell.
                //TableCell tc2 = new TableCell(tc1.OuterXml);

                //// Append the table cell to the table row.
                //tr.Append(tc2);

                //// Append the table row to the table.
                //table.Append(tr);

                // Append the table to the document.
               // body.Append(table);
                //table = new Table();
                //
                //for (var i = 0; i <= 5; i++)
                //{
                //    tr = new TableRow();
                //    for (var j = 0; j <= 5; j++)
                //    {
                //        var tc = new TableCell();
                //        tc.Append(new Paragraph(new Run(new Text(i.ToString() + " " + j.ToString()))));

                //        // Assume you want columns that are automatically sized.
                //        tc.Append(new TableCellProperties(
                //            new TableCellWidth { Type = TableWidthUnitValues.Auto }));

                //        tr.Append(tc);
                //    }
                //    table.Append(tr);
                //}
                //body.Append(table);

            }



        }
    }
}
